package com.shangrongbao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.shangrongbao.core.mapper")
public class ShangrongbaoCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShangrongbaoCoreApplication.class, args);
        System.out.println("尚融宝-core-项目启动成功");
    }

}
