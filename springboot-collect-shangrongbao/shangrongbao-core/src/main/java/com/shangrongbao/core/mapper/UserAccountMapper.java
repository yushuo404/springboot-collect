package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.UserAccount;

/**
 * <p>
 * 用户账户 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface UserAccountMapper extends BaseMapper<UserAccount> {

}
