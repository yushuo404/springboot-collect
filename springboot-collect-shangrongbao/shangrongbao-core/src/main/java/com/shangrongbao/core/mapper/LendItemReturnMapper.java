package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.LendItemReturn;

/**
 * <p>
 * 标的出借回款记录表 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface LendItemReturnMapper extends BaseMapper<LendItemReturn> {

}
