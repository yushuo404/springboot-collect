package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.OperateAction;

/**
 * <p>
 * 操作行为记录表 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface OperateActionMapper extends BaseMapper<OperateAction> {

}
