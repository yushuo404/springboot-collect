package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.TransFlow;

/**
 * <p>
 * 交易流水表 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface TransFlowMapper extends BaseMapper<TransFlow> {

}
