package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.UserLoginRecord;

/**
 * <p>
 * 用户登录记录表 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface UserLoginRecordMapper extends BaseMapper<UserLoginRecord> {

}
