package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.Borrower;

/**
 * <p>
 * 借款人 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface BorrowerMapper extends BaseMapper<Borrower> {

}
