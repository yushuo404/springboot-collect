package com.shangrongbao.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shangrongbao.core.pojo.entity.BorrowerAttach;

/**
 * <p>
 * 借款人上传资源表 Mapper 接口
 * </p>
 *
 * @author Helen
 * @since 2021-02-20
 */
public interface BorrowerAttachMapper extends BaseMapper<BorrowerAttach> {

}
