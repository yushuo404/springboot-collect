package com.shangrongbao.core.mapper;

import com.shangrongbao.core.pojo.entity.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CoreMapperTests {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    public void mapperTest01() {
        List<UserInfo> userInfos = userInfoMapper.selectList(null);
        System.out.println("6666"+userInfos);
    }
}
