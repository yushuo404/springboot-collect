package com.shangrongbao.common.result;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Result {

    private Integer code;
    private String message;
    private Map<String, Object> data = new HashMap<>();


    /**
     * 构造函数私有化
     */
    private Result(){}

    /**
     * 返回成功结果
     */
    public static Result ok(){
        Result result = new Result();
        result.setCode(ResponseEnum.SUCCESS.getCode());
        result.setMessage(ResponseEnum.SUCCESS.getMessage());
        return result;
    }



    /**
     * 返回失败结果
     */

    public static Result error(){
        Result result = new Result();
        result.setCode(ResponseEnum.ERROR.getCode());
        result.setMessage(ResponseEnum.ERROR.getMessage());
        return result;
    }

    /**
     * 设置特定的结果-曾宪旺-第一个设置枚举
     */
    public static Result setResult(ResponseEnum responseEnum){
        Result result = new Result();
        result.setCode(responseEnum.getCode());
        result.setMessage(responseEnum.getMessage());
        return result;
    }



    /**
     * 设置特定的结果-曾宪旺-第二个设置data
     */
    public Result data(String key,Object value){
        this.data.put(key, value);
        return this;
    }

    /**
     * 设置特定的结果-曾宪旺-第二个设置data的map
     */
    public Result data(Map<String, Object> map) {
        this.setData(map);
        return this;
    }

    /**
     * 设置特定的响应消息--message
     */
    public Result message(String message) {
        this.setMessage(message);
        return this;
    }

    /**
     * 设置特定的响应消息--code
     */
    public Result code(Integer code) {
        this.setCode(code);
        return this;
    }
}
