package com.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {


    Employee select001();

    //根据id查询员工信息
    Employee selectById001(Long id);

    //新增员工信息
    int saveEmployee(Employee employee);

}
