package com.reggie.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.reggie.entity.Employee;
import com.reggie.mapper.EmployeeMapper;
import com.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;
    @Override
    public Employee select001() {
        System.out.println("222222222222"+employeeMapper);
        return employeeMapper.selectById(1);
    }

    //根据id查询员工信息
    @Override
    public Employee selectById001(Long id) {
        log.info("service层 - 根据id查询员工信息...");
        Employee employee = employeeMapper.selectById(id);
        return employee;
    }

    //新增员工信息
    @Override
    public int saveEmployee(Employee employee) {
        int i = employeeMapper.updateById(employee);
        return i;
    }

}
