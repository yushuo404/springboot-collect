package com.reggie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReggieProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReggieProjectApplication.class, args);
        System.out.println("reggie项目启动成功");
    }

}
