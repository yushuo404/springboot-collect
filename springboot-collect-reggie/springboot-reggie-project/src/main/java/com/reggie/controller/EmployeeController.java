package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.reggie.common.Result;
import com.reggie.entity.Employee;
import com.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@RestController
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("test001")
    public String test001() {
        System.out.println("Controller类测试成功");
        return "Controller类测试成功";
    }

    @GetMapping("test002")
    public Employee test002() {
        Employee employee = employeeService.select001();
        System.out.println("Controller类测试成功");
        return employee;
    }

    @GetMapping("test003")
    public Result<Employee> test003() {
        Employee employee = employeeService.select001();
        System.out.println("Controller类测试成功");
        return Result.success(employee);
    }

    //根据id查询员工信息
    @GetMapping("{id}")
    public Result<Employee> selectById001(@PathVariable Long id) {
        Employee employee = employeeService.selectById001(id);
        if (employee == null) {
            return Result.error("查询员工失败");
        }
        return Result.success(employee);
    }

    //根据id查询员工信息
    @GetMapping("select/{id}")
    public Result<Employee> selectById002(@PathVariable Long id) {
        Employee employee = employeeService.selectById001(id);
        if (employee == null) {
            return Result.error("查询员工失败");
        }
        return Result.successInfo(employee, "查询员工成功");
    }

    //新增员工信息
    //结果显示：添加失败
    @PostMapping("/save001")
    public Result<String> saveEmployee001(@RequestBody Employee employee) {
        int i = employeeService.saveEmployee(employee);
        if (i < 1) {
            return Result.error("新增失败");
        }
        return Result.success("添加成功");
    }

    //新增员工信息
    //结果显示：添加失败
    @PostMapping("/save002")
    public Result<String> saveEmployee002(@RequestBody Employee employee) {
        boolean save = employeeService.save(employee);
        if (save) {
            return Result.success("添加成功");
        }
        return Result.error("新增失败");
    }

    //员工分页查询
    @GetMapping("/{pageSize}/{pageNo}/{name}")
    public Result<Page> page(@PathVariable int pageSize, @PathVariable int pageNo, @PathVariable String name) {
        log.info("pageNo = {},pageSize = {},name = {}" ,pageNo,pageSize,name);
        Page pageInfo = new Page<>(pageNo, pageSize);
        LambdaQueryWrapper<Employee> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(name),Employee::getName, name);
        wrapper.orderByDesc(Employee::getCreateTime);
        Page page = employeeService.page(pageInfo, wrapper);
        for (Object record : page.getRecords()) {
            for (int i = 1; i < 10; i++) {
                System.out.println("1111111---第"+i+"个for循环：--》"+record);
            }
        }
        System.out.println("2233"+page);
        return Result.success(pageInfo);
    }

    @GetMapping("/page")
    public Result<Page> page001( int pageSize,  int pageNo,  String name) {
        log.info("pageNo = {},pageSize = {},name = {}" ,pageNo,pageSize,name);
        Page pageInfo = new Page<>(pageNo, pageSize);
        LambdaQueryWrapper<Employee> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(name),Employee::getName, name);
        wrapper.orderByDesc(Employee::getCreateTime);
        Page page = employeeService.page(pageInfo, wrapper);
        List records = page.getRecords();
        System.out.println("1111111---第个for循环：--》"+records);
        for (Object record : page.getRecords()) {
            for (int i = 1; i < page.getRecords().size(); i++) {
                System.out.println("1111111---第"+i+"个for循环：--》"+record);
            }
        }
        System.out.println("2233"+page);
        return Result.success(pageInfo);
    }
}
