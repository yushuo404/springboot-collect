package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.Result;
import com.reggie.entity.Category;
import com.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    //新增分类
    @PostMapping
    public Result<String> saveCategory(@RequestBody Category category) {
        log.info("添加的详细参数:{}", category);
        categoryService.save(category);
        return Result.success("success");
    }

    //分页查询
    @GetMapping
    public Result<Page> page(int pagesize, int pageNo) {
        Page<Category> categoryPage = new Page<>(pageNo, pagesize);
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        Page<Category> page = categoryService.page(categoryPage, wrapper);
        for (Category record : page.getRecords()) {
            System.out.println("FOR循环：" + record);
        }
        return Result.success(categoryPage);
    }

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    @DeleteMapping
    public Result<String> delete(Long id){
        log.info("删除分类，id为：{}",id);

        categoryService.removeById(id);

        return Result.success("分类信息删除成功");
    }

    /**
     * 根据id修改分类信息
     * @param category
     * @return
     */
    @PutMapping
    public Result<String> update(@RequestBody Category category){
        log.info("修改分类信息：{}",category);

        categoryService.updateById(category);

        return Result.success("修改分类信息成功");
    }
}
