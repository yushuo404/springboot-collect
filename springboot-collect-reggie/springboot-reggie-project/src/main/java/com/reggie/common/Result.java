package com.reggie.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @param <T>
 */
@Data
public class Result<T> {
    private String code;
    private String message;
    private T data;
    private Map map = new HashMap();//动态数据

    public static <T> Result<T> success(T object) {
        Result<T> result = new Result<>();
        result.data = object;
        result.code = "成功";
        return result;
    }

    public static <T> Result<T> successInfo(T object,String message) {
        Result<T> result = new Result<>();
        result.data = object;
        result.message = message;
        result.code = "成功";
        return result;
    }

    public static <T> Result<T> error(String message) {
        Result result = new Result();
        result.message = message;
        result.code = "失败";
        return result;
    }

    public Result<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }
}
