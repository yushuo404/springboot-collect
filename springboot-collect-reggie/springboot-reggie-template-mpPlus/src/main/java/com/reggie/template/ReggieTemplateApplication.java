package com.reggie.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReggieTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReggieTemplateApplication.class, args);
    }

}
