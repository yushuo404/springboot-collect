package com.reggie.template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;
    @GetMapping("/test")
    public int test001() {
        int count = addressBookService.count();
        System.out.println("success---------------"+count);
        return count;
    }
}
