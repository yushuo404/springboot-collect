package com.xuecheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XuechengSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuechengSystemApplication.class, args);
        System.out.println("system模块启动成功");
    }

}
