package com.xuecheng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xuecheng.content.mapper")
public class XuechengContentApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuechengContentApplication.class, args);
        System.out.println("content模块启动成功");
    }

}
