package com.xuecheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XuechengBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuechengBaseApplication.class, args);
        System.out.println("base项目启动成功");
    }

}
