package com.java;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SpringbootCollectJavaBaseApplication.class)
public class SpringbootCollectJavaBaseApplicationTests {
    @Test
    void contextLoads() {
    }

}
