package com.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCollectJavaBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCollectJavaBaseApplication.class, args);
    }

}
