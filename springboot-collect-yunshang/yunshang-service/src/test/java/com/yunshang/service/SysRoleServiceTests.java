package com.yunshang.service;

import com.yunshang.YunshangServiceApplication;
import com.yunshang.YunshangServiceApplicationTests;
import com.yunshang.system.service.SysRoleService;
import com.yunshang.wechat.entity.RequestVo;
import com.yunshang.wechat.service.WechatFunctionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest(classes = YunshangServiceApplication.class)
public class SysRoleServiceTests {
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private WechatFunctionService wechatFunctionService;
    @Test
    public void test001() {
        System.out.println("test001");
    }
    @Test
    public void test002() {
        System.out.println(sysRoleService.list());
        System.out.println("test002");
    }
    @Test
    public void test003() {
        Map<String, Object> roleByAdmin = sysRoleService.findRoleByAdmin(2L);

        System.out.println("结果"+roleByAdmin);
        System.out.println("test002");
    }
    @Test
    public void test004() {
        RequestVo requestVo = new RequestVo();
        requestVo.setRequestId("123123");
        requestVo.setPrompt("22222");
        requestVo.setAccessKey("44444444");
        requestVo.setUserId("99");
        System.out.println("test001"+requestVo);
        System.out.println("test002");
    }
}
