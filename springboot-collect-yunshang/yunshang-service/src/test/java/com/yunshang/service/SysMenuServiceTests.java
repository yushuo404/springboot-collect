package com.yunshang.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yunshang.YunshangServiceApplication;
import com.yunshang.model.system.SysMenu;
import com.yunshang.model.system.SysRoleMenu;
import com.yunshang.system.mapper.SysMenuMapper;
import com.yunshang.system.mapper.SysRoleMenuMapper;
import com.yunshang.system.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest(classes = YunshangServiceApplication.class)
@Slf4j
public class SysMenuServiceTests {
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;
    @Test
    public void test001() {
        List<SysMenu> nodes = sysMenuService.findNodes();
        System.out.println("测试成功"+nodes);
    }

    @Test
    public void test002() {
        List<SysMenu> list = sysMenuService.list();
        for (SysMenu sysMenu : list) {
            long l = sysMenu.getParentId().longValue();
            System.out.println("111111111111111111-----------"+l);
        }
        System.out.println("测试成功"+list);
    }
    @Test
    public void test003() {
        SysMenu byId = sysMenuService.getById(8);
        long l = byId.getParentId().longValue();
        System.out.println("测试成功"+l);
    }
    @Test
    public void test004() {
        System.out.println("测试成功1");
        LambdaQueryWrapper<SysRoleMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysRoleMenu::getRoleId, 2);
        List<SysRoleMenu> sysRoleMenuList = sysRoleMenuMapper.selectList(queryWrapper);
        List<Long> menuIdList = sysRoleMenuList.stream().map(e -> e.getMenuId()
        ).collect(Collectors.toList());
        log.info("001-List:{}",menuIdList);
        System.out.println("测试成功2");
    }

}
