package com.yunshang.mapper;


import com.yunshang.YunshangServiceApplication;
import com.yunshang.system.mapper.SysRoleMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = YunshangServiceApplication.class)
public class SysRoleMapperTests {
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Test
    public void test001() {
        System.out.println("test001");
    }
    @Test
    public void test002() {
        System.out.println(sysRoleMapper.selectList(null));
        System.out.println("test002");
    }
}
