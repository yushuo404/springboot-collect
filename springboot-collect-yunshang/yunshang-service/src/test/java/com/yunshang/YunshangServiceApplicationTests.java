package com.yunshang;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.SQLException;


@SpringBootTest(classes = YunshangServiceApplication.class)
@Slf4j
public class YunshangServiceApplicationTests {

    @Autowired
    private DataSource dataSource;

    @Test
    public void test001() {
        System.out.println("test002");
    }
    @Test
    public void getConnection() throws SQLException {
        System.out.println(dataSource.getConnection());
    }
}
