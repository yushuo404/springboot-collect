package com.yunshang.mapperAndXmlTest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SrbUserMapper extends BaseMapper<ShangRongBaoUser> {
    @Select("SELECT * FROM shangrongbao_user")
    List<ShangRongBaoUser> selectAllByName();
}
