package com.yunshang.mapperAndXmlTest.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 测试自定义mapper类
 */
@Data
@TableName("shangrongbao_user")
public class ShangRongBaoUser {
    private int id;
    private String name;
    private String age;
}
