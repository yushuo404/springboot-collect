package com.yunshang.wechat.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunshang.wechat.entity.RequestEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RequestMapper extends BaseMapper<RequestEntity> {
}