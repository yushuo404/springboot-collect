package com.yunshang.wechat.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunshang.wechat.entity.RequestEntity;
import com.yunshang.wechat.mapper.RequestMapper;
import org.springframework.stereotype.Service;

@Service
public class RequestService extends ServiceImpl<RequestMapper, RequestEntity> implements IService<RequestEntity> {
}
