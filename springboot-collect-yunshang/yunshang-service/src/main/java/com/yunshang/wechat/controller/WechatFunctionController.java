package com.yunshang.wechat.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yunshang.wechat.entity.RequestEntity;
import com.yunshang.wechat.entity.RequestVo;
import com.yunshang.wechat.service.RequestService;
import com.yunshang.wechat.service.WechatFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("wechat")
public class WechatFunctionController {
    @Autowired
    private RequestService requestService;
    @Autowired
    private WechatFunctionService chatFunctionService;


    /**
     * 聊天接口
     * @param requestVo
     * @return
     */
    @PostMapping
    public String chatAi(@RequestBody RequestVo requestVo) {
        String result = null;
        try {
            result = chatFunctionService.wechatFunction(requestVo);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    /**
     * 获取历史请求列表
     */
    @GetMapping
    public List<String> historyRequest() {
        List<RequestEntity> list = requestService.list();
        List<RequestEntity> collect = list.stream().map((items) -> {
            items.getPrompt();
            return items;
        }).collect(Collectors.toList());
        List<String> promptList = collect.stream().map((items) -> {
            String prompt = items.getPrompt();
            return prompt;
        }).collect(Collectors.toList());
        return promptList;
    }

    /**
     * 模糊查询接口
     * @param keyword
     * @return
     */
    @GetMapping("/fuzzyQuery")
    public List<String> fuzzyQuery(String keyword) {
        LambdaQueryWrapper<RequestEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(RequestEntity::getPrompt, keyword).or().like(RequestEntity::getAnswer, keyword);
        wrapper.orderByDesc();
        List<RequestEntity> list = requestService.list(wrapper);
        List<String> promptList = list.stream().map((items) -> {
            String prompt = items.getPrompt();
            return prompt;
        }).collect(Collectors.toList());
        return promptList;
    }
}
