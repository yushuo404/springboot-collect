package com.yunshang.wechat.entity;

import lombok.Data;

@Data
public class RequestVo {
    //页面相关的入参请求
    private String prompt;
    private String userId;
    private String accessKey;
    private String requestId;
}
