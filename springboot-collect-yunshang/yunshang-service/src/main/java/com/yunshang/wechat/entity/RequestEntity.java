package com.yunshang.wechat.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("ai_chat_request")
public class RequestEntity extends ChatBaseEntity {

    private static final long serialVersionUID = 1L;

    //请求
    @TableField("prompt")
    private String prompt;

    //响应
    @TableField("answer")
    private String answer;

    //用户名
    @TableField("user_name")
    private String userName;

    //会话id
    @TableField("conversation_id")
    private String conversationId;
}
