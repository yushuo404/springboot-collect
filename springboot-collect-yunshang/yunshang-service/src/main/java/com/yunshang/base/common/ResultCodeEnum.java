package com.yunshang.base.common;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 *
 */
@Getter
public enum ResultCodeEnum {
    SUCCESS(200, "成功"),
    FAIL(911, "失败"),
    SERVICE_ERROR(811, "service服务异常"),
    DATA_ERROR(711, "DATA数据异常"),
    LOGIN_AUTH(611, "用户未登陆"),
    PERMISSION(511, "你这个用户没有权限");

    private Integer code;

    private String message;



    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
