package com.yunshang.base.utils;

import com.yunshang.model.system.SysMenu;
import com.yunshang.system.service.SysMenuService;

import java.util.ArrayList;
import java.util.List;

/**
 * 根据菜单数据构建菜单数据
 */
public class MenuHelper {
    /*
     * 使用递归方法建菜单
     */
    public static List<SysMenu> buildTree(List<SysMenu> sysMenuList) {
        List<SysMenu> trees = new ArrayList<>();
        //思路：入参是list，进行循环
        for (SysMenu items : sysMenuList) {
            if (items.getParentId().longValue() == 0) {
                System.out.println("99999999999999999999:" + items);
                trees.add(findChildren(items, sysMenuList));
            }
        }
        return trees;
    }

    /*
     * 递归查找子节点
     * @todo 下次再看到的时候写一遍
     */
    public static SysMenu findChildren(SysMenu sysMenu, List<SysMenu> sysMenuList) {
        List<SysMenu> list = new ArrayList<>();
        System.out.println("00000000看一下" + sysMenu);
        System.out.println("1111111111这个里面应该是空数据什么都没有" + list);
        sysMenu.setChildren(list);
        System.out.println("2222222222这个应该是四个父类parentId=0的下面的子类" + list);
        for (SysMenu it : sysMenuList) {
            if (sysMenu.getId().longValue() == it.getParentId().longValue()) {
                if (sysMenu.getChildren() == null) {
                    sysMenu.setChildren(new ArrayList<>());
                }
                //上面这个if表示如果children是null就给他一个空值，避免报错
                sysMenu.getChildren().add(findChildren((it), sysMenuList));
                //一下用于自我测试
                System.out.println("555555555555" + sysMenu.getChildren());
                System.out.println("9999999999999999999"+sysMenu.getChildren().add(findChildren((it), sysMenuList)));
            }
        }
        return sysMenu;
    }

    public static List<SysMenu> buildTree001(List<SysMenu> sysMenuList) {
        List<SysMenu> trees = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            if (sysMenu.getParentId() == 0) {
                trees.add(findChildren001(sysMenu, sysMenuList));
            }
        }
        return trees;
    }

    private static SysMenu findChildren001(SysMenu sysMenu, List<SysMenu> sysMenuList) {
        List<SysMenu> list = new ArrayList<>();
        System.out.println("00000000看一下" + sysMenu);
        System.out.println("1111111111这个里面应该是空数据什么都没有" + list);
        sysMenu.setChildren(list);
        //sysMenu.setChildren(new ArrayList<>());
        for (SysMenu it : sysMenuList) {
            if (sysMenu.getId().longValue() == it.getParentId().longValue()) {
                if (sysMenu.getChildren() == null) {
                    sysMenu.setChildren(new ArrayList<>());
                }
                sysMenu.getChildren().add(findChildren001(it, sysMenuList));
            }
        }
        return sysMenu;
    }
}
