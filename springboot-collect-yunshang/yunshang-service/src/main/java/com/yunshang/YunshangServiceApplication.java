package com.yunshang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YunshangServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(YunshangServiceApplication.class, args);
    }

}
