package com.yunshang.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yunshang.base.common.Result;
import com.yunshang.model.system.SysRole;
import com.yunshang.system.service.SysRoleService;
import com.yunshang.vo.system.SysRoleQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.events.Event;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("system/role")
@Api(value = "标题", tags = "标签一角色描述")
public class SystemRoleController {

    @Autowired
    private SysRoleService sysRoleService;


    @GetMapping("get01")
    @ApiOperation("测试get001")
    public String get01() {
        System.out.println("get01");
        return "get01";
    }

    @ApiOperation("根据用户获取角色数据001")
    @GetMapping("/assign/{useId}")
    public Result assign(@PathVariable Long useId) {
        Map<String, Object> roleByAdmin = sysRoleService.findRoleByAdmin(useId);
        return Result.ok(roleByAdmin);
    }

    @ApiOperation("查询所有的角色001")
    @GetMapping("findAll")
    public List<SysRole> findAll() {
        List<SysRole> list = sysRoleService.list();
        log.info("----------11111111:{}", list);
        System.out.println("----------2222222222" + list);
        return list;
    }

    //利用mybatis的条件分页查询
    @ApiOperation("条件分页查询")
    @GetMapping("{page}/{limit}")
    public Result pageQueryRole(@PathVariable Long page, Long limit, SysRoleQueryVo sysRoleQueryVo) {
        Page<SysRole> sysRolePage = new Page<>();
        log.info("----------11111111:{}", sysRolePage);

        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        String roleName = sysRoleQueryVo.getRoleName();
        wrapper.like(SysRole::getRoleName, roleName);
        Page<SysRole> rolePage = sysRoleService.page(sysRolePage, wrapper);//此处应该没有什么数据需要redrock
        System.out.println("分页结果0000000：" + rolePage);
        for (SysRole record : rolePage.getRecords()) {
            System.out.println("分页结果1111111：" + record);
        }
        return Result.ok(rolePage);
    }

    /**
     * 接下来开始增删改查的最基本的crud 的方法了。
     * 这个方法可以在最新的聊天的时候用。
     * <p>
     * 第1个：新增方法 添加角色
     * 第2个：查询方法 查询角色=根据id进行查询
     * 第3个：修改角色 查询角色=根据id进行查询
     * 第4个：删除 根据id进行删除还是根据ids批量删除
     * 第5个：删除 根据ids批量删除
     */
    //第1个：新增方法 添加角色
    @PostMapping("save")
    @ApiOperation("第1个：新增方法 添加角色")
    public Result save(@RequestBody SysRole sysRole) {
        boolean save = sysRoleService.save(sysRole);
        if (save) {
            System.out.println("数据保存成功");
            return Result.ok("数据保存成功");
        } else {
            return Result.fail("数据保存失败");
        }
    }

    //第2个：查询方法 查询角色=根据id进行查询
    @ApiOperation("第2个：查询方法 查询角色=根据id进行查询-聊天用的更新方法-根据id查询")
    @GetMapping("get/{id}")
    public Result get(@PathVariable Long id) {
        SysRole byId = sysRoleService.getById(id);
        return Result.ok(byId);
    }

    //第3个：修改角色 查询角色=根据id进行查询
    @ApiOperation("第3个：修改角色 查询角色=根据id进行查询-聊天用的修改的方法")
    @PostMapping("update")
    public Result update(@RequestBody SysRole sysRole) {
        boolean save = sysRoleService.updateById(sysRole);
        if (save) {
            SysRole data = sysRoleService.getById(sysRole.getId());
            return Result.ok("更新的数据"+data);
        } else {
            return Result.fail("更新失败原因：您所更新的原始数据不存在，请检查输入id");
        }
    }

    //第4个：删除 根据id进行删除还是根据ids批量删除
    //第5个：删除 根据ids批量删除
    //注意20231123曾宪旺此处用的逻辑删除-update方法
    @ApiOperation("聊天用的删除的方法批量删除")
    @PostMapping("delete_id/{ids}")
    public Result deleteByIds(@PathVariable List<Long> ids) {
        boolean save = sysRoleService.removeByIds(ids);
        if (save) {
            return Result.ok("success");
        } else {
            return Result.fail("fail");
        }
    }
}
