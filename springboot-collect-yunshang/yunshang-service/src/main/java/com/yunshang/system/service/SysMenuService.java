package com.yunshang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunshang.model.system.SysMenu;

import java.util.List;

public interface SysMenuService extends IService<SysMenu> {
    List<SysMenu> findNodes();

    List<SysMenu> findNodes0001();
}
