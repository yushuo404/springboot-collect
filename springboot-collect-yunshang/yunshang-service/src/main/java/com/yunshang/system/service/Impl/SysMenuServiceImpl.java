package com.yunshang.system.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunshang.base.utils.MenuHelper;
import com.yunshang.model.system.SysMenu;
import com.yunshang.system.mapper.SysMenuMapper;
import com.yunshang.system.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Autowired
    private SysMenuMapper sysMenuMapper;


    /*
     * 三级分类成功了--再重新写一遍。
     * */
    @Override
    public List<SysMenu> findNodes() {
        //全部权限列表
        List<SysMenu> sysMenuList = this.list();
        if (CollectionUtils.isEmpty(sysMenuList)) return null;
        //构建树形数据
        List<SysMenu> sysMenuTree = MenuHelper.buildTree(sysMenuList);
        return sysMenuTree;
    }

    @Override
    public List<SysMenu> findNodes0001() {
        List<SysMenu> sysMenuList = this.list();
        List<SysMenu> sysMenuTree001 = MenuHelper.buildTree001(sysMenuList);
        return sysMenuTree001;
    }
}
