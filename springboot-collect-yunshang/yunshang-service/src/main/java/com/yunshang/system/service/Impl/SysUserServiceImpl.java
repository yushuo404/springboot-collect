package com.yunshang.system.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunshang.model.system.SysUser;
import com.yunshang.system.mapper.SysUserMapper;
import com.yunshang.system.service.SysUserService;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
