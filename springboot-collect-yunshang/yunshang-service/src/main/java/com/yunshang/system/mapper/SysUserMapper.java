package com.yunshang.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunshang.model.system.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
}
