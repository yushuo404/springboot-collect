package com.yunshang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunshang.model.system.SysMenu;

import java.util.List;

public interface RoleManagementService extends IService<SysMenu> {
    //根据角色获取授权权限数据
    List<SysMenu> getListByRoleId(Long roleId);
}
