package com.yunshang.system.controller;

import com.yunshang.base.common.Result;
import com.yunshang.model.system.SysMenu;
import com.yunshang.system.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.SocketTimeoutException;
import java.util.List;

/**
 * 这个controller类中之写三级分类的内容并且三级分类需要掌握
 */
@Slf4j
@RestController
@RequestMapping("menu")
@Api(tags = "菜单管理曾宪旺")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;

    @GetMapping("test")
    public String controller001() {
        log.info("success");
        System.out.println(sysMenuService.list());
        return "success";
    }
    /*
    * 三级分类成功了，分析一下逻辑再写一遍
    * */
    @ApiOperation(value = "获取菜单")
    @GetMapping ("findNodes")
    public Result findNodes() {
        List<SysMenu> list = sysMenuService.findNodes();
        return Result.ok(list);
    }

    @ApiOperation(value = "获取菜单001COPY")
    @GetMapping ("findNodes001")
    public Result findNodes0001() {
        List<SysMenu> list = sysMenuService.findNodes0001();
        return Result.ok(list);
    }
}
