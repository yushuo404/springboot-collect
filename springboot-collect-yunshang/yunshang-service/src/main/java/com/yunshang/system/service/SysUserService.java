package com.yunshang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunshang.model.system.SysUser;

public interface SysUserService extends IService<SysUser> {

}
