package com.yunshang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunshang.model.system.SysRole;

import java.util.Map;

public interface SysRoleService extends IService<SysRole> {

    Map<String, Object> findRoleByAdmin(Long useId);
}
