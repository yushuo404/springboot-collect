package com.yunshang.system.controller;

import com.yunshang.base.common.Result;
import com.yunshang.model.system.SysMenu;
import com.yunshang.system.service.RoleManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@Api(tags = "角色管理")
@RequestMapping("role/management")
public class RoleManagementController {
    @Autowired
    private RoleManagementService roleManagementService;

    @ApiOperation("第一个测试方法-根据角色获取菜单")
    @RequestMapping(value = "test",method = RequestMethod.GET)
    public Result test001() {
        System.out.println("test");
        return Result.ok("success");
    }

    @ApiOperation("根据角色ID获取菜单List")
    @RequestMapping(value = "assign/{id}",method = RequestMethod.GET)
    public Result toAssign(@PathVariable Long roleId) {
        System.out.println("test");
        List<SysMenu> sysMenuList = roleManagementService.getListByRoleId(roleId);
        return Result.ok("success");
    }
}
