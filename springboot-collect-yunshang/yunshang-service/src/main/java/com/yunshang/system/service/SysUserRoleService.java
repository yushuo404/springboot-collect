package com.yunshang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunshang.model.system.SysUserRole;

public interface SysUserRoleService extends IService<SysUserRole> {
}
