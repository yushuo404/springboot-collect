package com.yunshang.system.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.yunshang.base.common.Result;
import com.yunshang.model.system.SysRole;
import com.yunshang.model.system.SysUser;
import com.yunshang.model.system.SysUserRole;
import com.yunshang.system.mapper.SysRoleMapper;
import com.yunshang.system.mapper.SysUserMapper;
import com.yunshang.system.mapper.SysUserRoleMapper;
import com.yunshang.system.service.SysRoleService;
import com.yunshang.system.service.SysUserRoleService;
import com.yunshang.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private SysUserMapper sysUserMapper;
    /*

     */

    /**
     * 根据用户获取角色数据
     * @param useId
     * @return
     */
    @Override
    public Map<String, Object> findRoleByAdmin(Long useId) {

         /*
         001-查询所有的角色
         如何实现查询所有的角色，我知道一个方法就是list-但是你现在所在的层是service层所以用自己层级的东西就行了。
         */
        List<SysRole> list = this.list();
        /*
        通过userId构建查询
        */
        LambdaQueryWrapper<SysUserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUserRole::getId, useId);
        wrapper.select(SysUserRole::getRoleId);
        List<SysUserRole> sysUserRoles = sysUserRoleMapper.selectList(wrapper);
        System.out.println("-------002------" + sysUserRoles);
        List<Object> collect = sysUserRoles.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        System.out.println("-------003------" + collect);
        List<SysRole> SystemRoleList = new ArrayList<>();
        System.out.println("-------004------" + SystemRoleList);
        for (SysRole role : SystemRoleList) {
            if (collect.contains(role.getId())) {
                SystemRoleList.add(role);
            }
        }
        System.out.println("-------005------" + SystemRoleList);
        HashMap<String, Object> roleMap = new HashMap<>();
        roleMap.put("assginRoleList", SystemRoleList);
        System.out.println("-------006------" + roleMap);
        roleMap.put("allrolesList", collect);
        return roleMap;
    }

}
