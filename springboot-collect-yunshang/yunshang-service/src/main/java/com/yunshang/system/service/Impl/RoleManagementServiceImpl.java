package com.yunshang.system.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunshang.model.system.SysMenu;
import com.yunshang.model.system.SysRoleMenu;
import com.yunshang.system.mapper.RoleManagementMapper;
import com.yunshang.system.mapper.SysRoleMenuMapper;
import com.yunshang.system.service.RoleManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RoleManagementServiceImpl extends ServiceImpl<RoleManagementMapper, SysMenu> implements RoleManagementService {
    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;
    @Override
    public List<SysMenu> getListByRoleId(Long roleId) {
        //第一步：全部权限列表
        List<SysMenu> list = this.list();
        //全部权限列表  ---当 status等于1的时候 结果是一样的
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysMenu::getStatus, 1);
        List<SysMenu> allSysMenuList = this.list(wrapper);


        //第二步：根据角色id获取角色权限

        LambdaQueryWrapper<SysRoleMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysRoleMenu::getRoleId, roleId);
        List<SysRoleMenu> sysRoleMenuList = sysRoleMenuMapper.selectList(queryWrapper);
        //第三步：转换给角色id与角色权限对应Map对象
        List<Long> menuIdList = sysRoleMenuList.stream().map(e -> e.getMenuId()
        ).collect(Collectors.toList());
        log.info("001-List:{}",menuIdList);
        allSysMenuList.forEach(permission->{
            if (menuIdList.contains(permission.getId())) {
                permission.setSelect(true);
            } else {
                permission.setSelect(true);
            }
        });
        //@TODO 不想写了
        return null;
    }
}
