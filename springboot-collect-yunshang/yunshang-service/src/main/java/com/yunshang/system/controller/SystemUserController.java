package com.yunshang.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yunshang.base.common.Result;
import com.yunshang.model.system.SysUser;
import com.yunshang.system.service.SysUserService;
import com.yunshang.vo.system.SysUserQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.events.Event;

@RestController
@RequestMapping("system/user")
@Api(value = "标题", tags = "用户描述")
public class SystemUserController {
    @Autowired
    private SysUserService sysUserService;

    @GetMapping("page/{page}/{limit}")
    @ApiOperation("用户条件分页查询")
    public Result page(@PathVariable Long page, @PathVariable Long limit, SysUserQueryVo sysUserQueryVo) {
        Page<SysUser> systemUserPage = new Page<>(page,limit);
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        //现在有一个功能性的需求，通过时间段进行查询
        String createTimeBegin = sysUserQueryVo.getCreateTimeBegin();
        String createTimeEnd = sysUserQueryVo.getCreateTimeEnd();
        if (!StringUtils.isEmpty(createTimeBegin)) {
            wrapper.ge(SysUser::getCreateTime, createTimeBegin);
        }
        String keyword = sysUserQueryVo.getKeyword();
        if (!StringUtils.isEmpty(keyword)) {
            wrapper.like(SysUser::getName, keyword);
        }
        Page<SysUser> userPage = sysUserService.page(systemUserPage, wrapper);
        return Result.ok(userPage);
    }
}
