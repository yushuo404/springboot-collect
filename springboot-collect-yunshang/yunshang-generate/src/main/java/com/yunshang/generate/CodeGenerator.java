package com.yunshang.generate;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.platform.commons.util.StringUtils;

import java.util.ArrayList;
import java.util.Scanner;
 
public class CodeGenerator {

    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }


    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
 
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        //每次的这个地方都需要更改------更改位置--003
        //注意这个地址需要自己再手动输入一边，不然不行
        //String generatePath = "D:\\桌面\\zxw\\011-云尚办公-自己git下载-加入版本管理\\base\\013\\008-20230524-zxw\\guigu-oa-parent\\service-oa";
        String generatePath = "D:\\桌面\\zxw\\guigu-oa-parent\\service-oa";
        //我把projectPath换成了generatePath
        gc.setOutputDir(generatePath + "/src/main/java");//设置代码生成路径
        gc.setFileOverride(true);//是否覆盖以前文件
        gc.setOpen(false);//是否打开生成目录
        gc.setAuthor("zxw");//设置项目作者名称
        gc.setIdType(IdType.AUTO);//设置主键策略
        gc.setBaseResultMap(true);//生成基本ResultMap
        gc.setBaseColumnList(true);//生成基本ColumnList
        gc.setServiceName("%sService");//去掉服务默认前缀
        gc.setDateType(DateType.ONLY_DATE);//设置时间类型
        mpg.setGlobalConfig(gc);
 
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/guigu-oa?useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        mpg.setDataSource(dsc);
 
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.atguigu.auth");
        pc.setModuleName("menu");//更改文件夹模块名称-002
        pc.setMapper("mapper");
        pc.setXml("mapper.xml");
        //pc.setEntity("entity");//生成实体类，如果不想让生成实体类可以注释掉，如果生成的实体类和表名不一样，需要在sc.setTablePrefix("oa_"); 中删除相应的表名前缀
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setController("controller");
        mpg.setPackageInfo(pc);
 
        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        sc.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
        sc.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
        sc.setEntityLombokModel(true);//自动lombok// lombok 模型 @Accessors(chain = true) setter链式操作
        sc.setRestControllerStyle(true);//restful api风格控制器
        sc.setControllerMappingHyphenStyle(true);//url中驼峰转连字符
        sc.setLogicDeleteFieldName("deleted");//设置逻辑删除
        //设置自动填充配置
        TableFill gmt_create = new TableFill("create_time", FieldFill.INSERT);
        TableFill gmt_modified = new TableFill("update_time", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills=new ArrayList<>();
        tableFills.add(gmt_create);
        tableFills.add(gmt_modified);
        sc.setTableFillList(tableFills);
 
        //乐观锁
        sc.setVersionFieldName("version");
        sc.setRestControllerStyle(true);//驼峰命名

       //sc.setTablePrefix("tbl_"); 设置表名前缀
        sc.setTablePrefix("wechat_"); //去掉前缀004这个也是每一个都要有的很重要
        //直接配置表名，配置那个表就生成那个表-001
        sc.setInclude("wechat_menu");
        //以下定义表名是需要通过手动输入，输入那个表就会生成那个表
        //sc.setInclude(scanner("表名，多个英文逗号分割").split(","));
        mpg.setStrategy(sc);
        System.out.println("代码生成成功-位置："+generatePath);
        // 生成代码
        mpg.execute();
    }

}