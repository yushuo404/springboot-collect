package com.yunshang.generate;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class Generate {

    public static void main(String[] args) {

        // 1、创建代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 2、全局配置
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //下面这个地址需要时刻修改
        gc.setOutputDir("D:\\桌面\\zxw\\011-云尚办公-自己git下载-加入版本管理\\013\\008-20230524-zxw\\guigu-oa-parent\\service-oa"+"/src/main/java");

        gc.setFileOverride(true);//是否覆盖以前的文件
        gc.setServiceName("%sService");	//去掉Service接口的首字母I
        gc.setAuthor("zxw");//设置项目作者名称
        gc.setIdType(IdType.AUTO);//设置主键策略
        gc.setBaseResultMap(true);//生成基本ResultMap
        gc.setDateType(DateType.ONLY_DATE);//设置时间类型
        gc.setOpen(false);//是否打开生成目录
        mpg.setGlobalConfig(gc);

        // 3、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/guigu-oa?serverTimezone=GMT%2B8&useSSL=false");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 4、包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.atguigu.auth");
        pc.setModuleName("zxw"); //模块名
        pc.setController("controller");
        pc.setService("service");
        pc.setMapper("mapper");
        pc.setEntity("entity");
        mpg.setPackageInfo(pc);

        // 5、策略配置
        StrategyConfig strategy = new StrategyConfig();

        //输入那个表名就会生成那个表名的plus
        strategy.setInclude("wechat_menu");

        //去掉前缀
        strategy.setTablePrefix("wechat_");

        strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略

        strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
        strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作

        strategy.setRestControllerStyle(true); //restful api风格控制器
        strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符

        mpg.setStrategy(strategy);

        // 6、执行
        mpg.execute();

    }
    /**
     *         <!-- junit.platform的依赖，不然会报错-->
     *         <dependency>
     *             <groupId>org.junit.platform</groupId>
     *             <artifactId>junit-platform-commons</artifactId>
     *             <version>1.9.3</version>
     *         </dependency>
     *                 <!-- 数据库驱动 -->
     *         <dependency>
     *             <groupId>mysql</groupId>
     *             <artifactId>mysql-connector-java</artifactId>
     *         </dependency>
     *
     *         <!-- lombok -->
     *         <dependency>
     *             <groupId>org.projectlombok</groupId>
     *             <artifactId>lombok</artifactId>
     *         </dependency>
     *
     *         <!-- mybatis-plus -->
     *         <dependency>
     *             <groupId>com.baomidou</groupId>
     *             <artifactId>mybatis-plus-boot-starter</artifactId>
     *             <version>3.0.5</version>
     *         </dependency>
     *
     *         <!-- 代码自动生成器依赖-->
     *         <dependency>
     *             <groupId>com.baomidou</groupId>
     *             <artifactId>mybatis-plus-generator</artifactId>
     *             <version>3.0.5</version>
     *         </dependency>
     *
     *         <dependency>
     *             <groupId>org.apache.velocity</groupId>
     *             <artifactId>velocity-engine-core</artifactId>
     *             <version>2.2</version>
     *         </dependency>
     *
     */
}
